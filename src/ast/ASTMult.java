package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTIntType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.IValue;
import value.IntValue;

public class ASTMult implements ASTNode<IValue>{
	
	private ASTNode<IValue> right;
	private ASTNode<IValue> left;
	
	public ASTMult (ASTNode<IValue> left, ASTNode<IValue> right) {
		this.right = right;
		this.left = left;
	}
	
	@Override
	public IValue<Integer> eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {
		IValue<Integer> v1 = left.eval(e, mem);
		if(v1 instanceof IntValue) {
			IValue<Integer> v2 = right.eval(e, mem);
			if(v2 instanceof IntValue) {
				return new IntValue(((IntValue)v1).getValue() * ((IntValue)v2).getValue());
			}
		}
		throw new TypeErrorException("Erro no tipo da expressão");
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		ASTType t = ASTIntType.thisType;
		
		if(!right.typecheck(e).equals(t) || !left.typecheck(e).equals(t))
			throw new TypeErrorException();
		
		return t;
	}
	
	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		left.compile(code, env);
		right.compile(code, env);
		code.emit("imul");
	}
}