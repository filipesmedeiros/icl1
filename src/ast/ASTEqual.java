package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTBoolType;
import type.ASTIntType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.BoolValue;
import value.IValue;
import value.IntValue;

public class ASTEqual implements ASTNode<IValue>{
	
	private ASTNode<IValue> right;
	private ASTNode<IValue> left;
	
	public ASTEqual (ASTNode<IValue> left, ASTNode<IValue> right) {
		this.right = right;
		this.left = left;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {
		IValue v1 = right.eval(e, mem);
		IValue v2 = left.eval(e, mem);
		
		return v1.equals(v2);
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		ASTType t = ASTIntType.thisType;
		ASTType t2 = ASTBoolType.thisType;
		
		if((right.typecheck(e).equals(t) && left.typecheck(e).equals(t)) ||
				right.typecheck(e).equals(t2) && left.typecheck(e).equals(t2))
					return t2;
		
		throw new TypeErrorException();
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		String labelTrue = code.label();
		String labelFalse = code.label();
		
		left.compile(code, env);
		right.compile(code, env);
		code.emit("isub");
		code.emit("ifeq", labelTrue);
		code.emit_bool(false);
		code.emit("goto", labelFalse);
		code.emit_label(labelTrue); code.emit_bool(true);
		code.emit_label(labelFalse);
	}
}
