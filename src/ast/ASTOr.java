package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTBoolType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.BoolValue;
import value.IValue;
import value.IntValue;

public class ASTOr implements ASTNode<IValue>{
	
	private ASTNode<IValue> right;
	private ASTNode<IValue> left;
	
	public ASTOr (ASTNode<IValue> left, ASTNode<IValue> right) {
		this.right = right;
		this.left = left;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {
		IValue v1 = left.eval(e, mem);
		if(v1 instanceof BoolValue) {
			IValue v2 = right.eval(e, mem);
			if(v2 instanceof BoolValue) {
				return new BoolValue(((BoolValue)v1).getValue() || ((BoolValue)v2).getValue());
			}
		}
		throw new TypeErrorException("Erro no tipo da expressão");
	}
	
	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		ASTType t = ASTBoolType.thisType;
		
		if(!right.typecheck(e).equals(t) || !left.typecheck(e).equals(t))
			throw new TypeErrorException();
		
		return t;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		left.compile(code, env);
		right.compile(code, env);
		code.emit("ior");
	}
}