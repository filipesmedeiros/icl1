package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTIntType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import value.IValue;
import value.IntValue;

public class ASTNum implements ASTNode<IValue>{
	
	private int num;
	
	public ASTNum (int num) {
		this.num = num;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException {
			return new IntValue(num);
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		return ASTIntType.thisType;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		code.emit_push(num);
	}
}