package ast;

import compiler.CodeBlock;
import memory.MemoryCell;
import memory.MemoryManagement;
import type.ASTRefType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.IValue;

public class ASTAttrib implements ASTNode<IValue> {
	
	private ASTNode<IValue> var;
	private ASTNode<IValue> value;
	
	public ASTType type;

	public ASTAttrib(ASTNode var, ASTNode value) {
		this.var = var;
		this.value = value;
	}

	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem)
			throws TypeErrorException, UnmatchedIdentifierException {
		
		if(!(var.eval(e, mem) instanceof MemoryCell))
			throw new TypeErrorException("Not variable");
		
		return mem.set(((MemoryCell) var.eval(e, mem)), value.eval(e, mem));
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		if(!(var.typecheck(e) instanceof ASTRefType))
			throw new TypeErrorException();
		
		ASTType typeRight = value.typecheck(e);
		
		if(!typeRight.equals(var.typecheck(e)))
			throw new TypeErrorException();
		
		return type = typeRight;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		var.compile(code, env);
		
		
	}

}
