package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTBoolType;
import type.ASTIntType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.IValue;

public class ASTInit implements ASTNode<IValue> {

	private ASTNode<IValue> fact;
	public ASTType type;
	
	public ASTInit(ASTNode<IValue> fact) {
		this.fact = fact;
	}
	
	@Override
	public IValue eval(IEnvironment e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {
		return mem.create(fact.eval(e, mem));
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		return type = fact.typecheck(e);
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		String t = null;
		if(type instanceof ASTIntType || type instanceof ASTBoolType)
			t = "ref_int";
		else
			t = "ref_class";
		
		code.emit_new(t);
		code.emit_dup();
		code.emit_invokeSpecial(t);
		code.emit_dup();
		fact.compile(code, env);
		code.emit_putField(t, "v", t.equals("ref_int") ? "I" : "java/lang/Object");
	}

}
