package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTBoolType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.BoolValue;
import value.IValue;
import value.IntValue;

public class ASTNot implements ASTNode<IValue>{
	
	private ASTNode<IValue> b;
	
	public ASTNot (ASTNode<IValue> b) {
		this.b = b;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {
		IValue v1 = b.eval(e, mem);
		
		if(v1 instanceof BoolValue ) {
			
			return new BoolValue(!((BoolValue)v1).getValue());
		}else {
			throw new TypeErrorException();
		}	
	}
	
	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		ASTType t = ASTBoolType.thisType;
		
		if(!b.typecheck(e).equals(t))
			throw new TypeErrorException();
		
		return t;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		String lbt = code.label();
		String lbf = code.label();
		
		b.compile(code, env);
		code.emit("ifeq", lbt);
		code.emit_bool(false);
		code.emit("goto", lbf);
		code.emit_label(lbt); code.emit_bool(true);
		code.emit_label(lbf);
	}
}