package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTBoolType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import value.BoolValue;
import value.IValue;

public class ASTBool implements ASTNode<IValue>{
	private boolean b;
	
	public ASTBool (boolean b) {
		this.b = b;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException {
			return new BoolValue(b);
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		return ASTBoolType.thisType;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		code.emit_bool(b);
	}
}
