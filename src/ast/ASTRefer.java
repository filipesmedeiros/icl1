package ast;

import compiler.CodeBlock;
import memory.MemoryCell;
import memory.MemoryManagement;
import type.ASTRefType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.IValue;

public class ASTRefer implements ASTNode<IValue> {
	
	private ASTNode<IValue> fact;

	public ASTRefer(ASTNode<IValue> fact) {
		this.fact = fact;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem)
			throws TypeErrorException, UnmatchedIdentifierException {
		if(!(fact.eval(e, mem) instanceof MemoryCell))
			throw new TypeErrorException("");
		
		return mem.get((MemoryCell)(fact.eval(e, mem)));
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		return fact.typecheck(e);
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		// TODO Auto-generated method stub
		
	}

}
