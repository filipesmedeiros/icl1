package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTBoolType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.Environment;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.BoolValue;
import value.IValue;
import value.IntValue;

public class ASTIf implements ASTNode<IValue>{
	
	private ASTNode<IValue> cond;
	private ASTNode<IValue> doIf;
	private ASTNode<IValue> doElse;
	
	public ASTIf (ASTNode<IValue> cond, ASTNode<IValue> doIf, ASTNode<IValue> doElse) {
		this.cond = cond;
		this.doIf = doIf;
		this.doElse = doElse;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {
		IValue v1 = cond.eval(e, mem);
		if(v1 instanceof BoolValue) {
			boolean b = (Boolean) v1.getValue();
			if(b) {
				IEnvironment<IValue> e2 = e.beginScope();
				IValue val = doIf.eval(e2, mem);
				e2.endScope();
				return val;
			} else {
				IEnvironment<IValue> e2 = e.beginScope();
				IValue val = doElse.eval(e2, mem);
				e2.endScope();
				return val;
			}
			
		}
		throw new TypeErrorException("Erro no tipo da expressão");
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		if(!cond.typecheck(e).equals(ASTBoolType.thisType))
			throw new TypeErrorException();
		
		ASTType type = doIf.typecheck(e);
		
		if(type != doElse.typecheck(e))
			throw new TypeErrorException();
		
		return type;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		String labelTrue = code.label();
		String labelFalse = code.label();
		
		cond.compile(code, env);
		code.emit("ifeq", labelTrue);
		doElse.compile(code, env);
		code.emit("goto", labelFalse);
		code.emit_label(labelTrue); doIf.compile(code, env);
		code.emit_label(labelFalse);
	}
	
	
}
