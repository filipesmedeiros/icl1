package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTIntType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.IValue;
import value.IntValue;

public class ASTNegative implements ASTNode<IValue>{
	
	private ASTNode<IValue> num;
	
	public ASTNegative (ASTNode<IValue> num) {
		this.num = num;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {
		IValue v1 = num.eval(e, mem);
		
		if(v1 instanceof IntValue ) {
			
			return new IntValue(-((IntValue)v1).getValue());
		}else {
			throw new TypeErrorException();
		}	
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		ASTType t = ASTIntType.thisType;
		
		if(!num.typecheck(e).equals(t))
			throw new TypeErrorException();
		
		return t;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		num.compile(code, env);
		code.emit("ineg");
	}
}