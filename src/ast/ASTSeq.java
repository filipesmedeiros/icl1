package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.IValue;

public class ASTSeq implements ASTNode<IValue>{
	
	private ASTNode<IValue> left;
	private ASTNode<IValue> right;
	
	public ASTSeq(ASTNode<IValue> left, ASTNode<IValue> right) {
		this.left = left;
		this.right = right;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {
		left.eval(e, mem);
		return right.eval(e, mem);
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		left.typecheck(e);
		return right.typecheck(e);
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		left.compile(code, env);
		right.compile(code, env);
	}
}
