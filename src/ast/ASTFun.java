package ast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTFunType;
import type.ASTType;
import utils.Address;
import utils.AlreadyExistsException;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.Closure;
import value.Closure.Param;
import value.IValue;

public class ASTFun implements ASTNode<IValue> {
	
	private List<Param> params;
	private ASTNode<IValue> body;
	
	public ASTFun() {
		params = new ArrayList<Param>();
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem)
			throws TypeErrorException, UnmatchedIdentifierException {
		return new Closure(params, body, e);
	}

	public void giveParam(Param param) {
		params.add(param);
	}
	
	public void giveBody(ASTNode<IValue> body) {
		this.body = body;
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		IEnvironment<ASTType> newE = e.beginScope();
		LinkedList<ASTType> parTypes = new LinkedList<ASTType>();
		
		for(Param p: params) {
			try {
				newE.assoc(p.name, p.type);
			} catch(AlreadyExistsException ex) {
				
			}
			parTypes.add(p.type);
		}
		
		return new ASTFunType(parTypes, body.typecheck(newE));
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		// TODO Auto-generated method stub
		
	}
}
