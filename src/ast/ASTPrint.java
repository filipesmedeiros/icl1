package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTType;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.IValue;

public class ASTPrint implements ASTNode {
	
	private ASTNode exp;
	
	public ASTPrint(ASTNode exp) {
		this.exp = exp;
	}

	@Override
	public IValue eval(IEnvironment e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {
		IValue val = exp.eval(e, mem);
		System.out.println(val.getValue().toString());
		return val;
	}

	@Override
	public ASTType typecheck(IEnvironment e) throws TypeErrorException {
		return exp.typecheck(e);
	}

	@Override
	public void compile(CodeBlock code, IEnvironment env) throws DuplicateIdentifierException {
		// TODO Auto-generated method stub
	}

}
