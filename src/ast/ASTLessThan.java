package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTBoolType;
import type.ASTIntType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.BoolValue;
import value.IValue;
import value.IntValue;

public class ASTLessThan implements ASTNode<IValue>{
	
	private ASTNode<IValue> right;
	private ASTNode<IValue> left;
	
	public ASTLessThan (ASTNode<IValue> left, ASTNode<IValue> right) {
		this.right = right;
		this.left = left;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {
		IValue v1 = left.eval(e, mem);
		if(v1 instanceof IntValue) {
			IValue v2 = right.eval(e, mem);
			if(v2 instanceof IntValue) {
				return new BoolValue(((IntValue)v1).getValue() < ((IntValue)v2).getValue());
			}
		}
		throw new TypeErrorException("Erro no tipo da expressão (booleana)");
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		ASTType t = ASTIntType.thisType;
		
		if(!right.typecheck(e).equals(t) || !left.typecheck(e).equals(t))
			throw new TypeErrorException();
		
		return ASTBoolType.thisType;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		String labelTrue = code.label();
		String labelFalse = code.label();
		
		left.compile(code, env);
		right.compile(code, env);
		code.emit("isub");
		code.emit("iflt", labelTrue);
		code.emit_bool(false);
		code.emit("goto", labelFalse);
		code.emit_label(labelTrue); code.emit_bool(true);
		code.emit_label(labelFalse);
	}
}
