package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTBoolType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.BoolValue;
import value.IValue;
import value.IntValue;

public class ASTWhile implements ASTNode<IValue>{

	private ASTNode<IValue> cond;
	private ASTNode<IValue> doWhile;

	public ASTWhile (ASTNode<IValue> cond, ASTNode<IValue> doWhile) {
		this.cond = cond;
		this.doWhile = doWhile;
	}

	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {
		IValue condition = cond.eval(e, mem);
		if(condition instanceof BoolValue) {
			while((boolean) cond.eval(e, mem).getValue()) {
				IEnvironment<IValue> e2 = e.beginScope();
				doWhile.eval(e2, mem);
				e2.endScope();
			}
		}
		
		return new BoolValue(false);
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		doWhile.typecheck(e);
		if(!cond.typecheck(e).equals(ASTBoolType.thisType))
			throw new TypeErrorException();
		
		return ASTBoolType.thisType;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		String label = code.label();
		String label1 = code.label();
		String label2 = code.label();
		
		code.emit_label(label);
		cond.compile(code, env);
		code.emit("ifeq", label1);
		code.emit("goto", label2);
		code.emit_label(label1);
		doWhile.compile(code, env);
		code.emit("goto", label);
		code.emit_label(label2);
	}
}
