package ast;

import java.util.ArrayList;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTType;
import utils.Address;
import utils.AlreadyExistsException;
import utils.Assoc;
import utils.Declaration;
import utils.DuplicateIdentifierException;
import utils.Environment;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.BoolValue;
import value.IValue;
import value.IntValue;

public class ASTLet implements ASTNode<IValue>{

	private ASTNode<IValue> in;
	private ArrayList<Declaration> vars;

	public ASTLet() {
		vars = new ArrayList<Declaration>();
	}

	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {

		IEnvironment<IValue> e2 = e.beginScope();

		for(Declaration var : vars)
			try {
				e2.assoc(var.id, var.value.eval(e2, mem));
			} catch (AlreadyExistsException e1) {
				System.out.println("Variable already exists.");
			}

		IValue ret = in.eval(e2, mem);

		e.endScope();

		return ret;
	}

	public void newVariable(String id, ASTNode value, ASTType type) {
		vars.add(new Declaration(id, value, type));
		System.out.println(id + " " + value.toString());
	}

	public void giveIn(ASTNode<IValue> in) {
		this.in = in;
	}

	@Override
	public ASTType typecheck(final IEnvironment<ASTType> e) throws TypeErrorException {
		IEnvironment<ASTType> typeEnv = e.beginScope();

		for(Declaration var : vars) {
			try {
				ASTType type = var.value.typecheck(typeEnv);
				
				if(!var.type.equals(type))
					throw new TypeErrorException();
				
				typeEnv.assoc(var.id, var.type);
			} catch (AlreadyExistsException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (TypeErrorException e1) {
				throw e1;
			}
		}

		ASTType t = in.typecheck(typeEnv);
		typeEnv.endScope();
		return t;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		
	}
}