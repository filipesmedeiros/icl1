package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.IValue;

public interface ASTNode<T> {
	
	IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException;
	
	ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException;
	
	void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException;
}

