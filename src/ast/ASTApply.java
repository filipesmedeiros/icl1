package ast;

import java.util.ArrayList;
import java.util.List;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTFunType;
import type.ASTType;
import utils.Address;
import utils.AlreadyExistsException;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.Closure;
import value.Closure.Param;
import value.IValue;

public class ASTApply implements ASTNode<IValue> {

	private ASTNode<IValue> id;
	private List<ASTNode<IValue>> args;
	
	public ASTApply(ASTNode<IValue> id) {
		this.id = id;
		args = new ArrayList<ASTNode<IValue>>();
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem)
			throws TypeErrorException, UnmatchedIdentifierException {
		Closure closure = (Closure) id.eval(e, mem);
		List<Param> params = closure.getParams();
		IEnvironment<IValue> e2 = closure.getE();
		IEnvironment<IValue> envlocal = e2.beginScope();
		for(int i = 0; i < params.size(); i++)
			try {
				envlocal.assoc(params.get(i).name, args.get(i).eval(envlocal, mem));
			} catch (AlreadyExistsException e1) {
			}
		
		IValue value = closure.getBody().eval(envlocal, mem);
		
		envlocal.endScope();
		
		return value;
	}

	public void addArg(ASTNode<IValue> arg) {
		args.add(arg);
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		ASTType type = id.typecheck(e);
		
		if(!(type instanceof ASTFunType))
			throw new TypeErrorException();
		
		ASTFunType fun = (ASTFunType) type;
		
		for(int i = 0; i < args.size(); i++) {
			if(!args.get(i).typecheck(e).equals(fun.params.get(i)))
				throw new TypeErrorException();
		}
		
		return fun.ret;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		// TODO Auto-generated method stub
		
	}
}