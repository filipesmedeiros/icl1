package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTType;
import utils.*;
import value.IValue;

public class ASTId implements ASTNode<IValue>{
	
	public String id;
	
	public ASTId (String id) {
		this.id = id;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws UnmatchedIdentifierException {
		IValue var = e.find(id);
		return var;
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		IEnvironment<ASTType> newenv = e.beginScope();
		ASTType val = null;
		try {
			val = e.find(id);
		} catch (UnmatchedIdentifierException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
		e = newenv.endScope();
		return val;
	}

	@Override
	public void compile(CodeBlock code, utils.IEnvironment<Address> env) throws DuplicateIdentifierException {
		// TODO Auto-generated method stub
		
	}

}
