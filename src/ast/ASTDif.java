package ast;

import compiler.CodeBlock;
import memory.MemoryManagement;
import type.ASTBoolType;
import type.ASTIntType;
import type.ASTType;
import utils.Address;
import utils.DuplicateIdentifierException;
import utils.IEnvironment;
import utils.TypeErrorException;
import utils.UnmatchedIdentifierException;
import value.BoolValue;
import value.IValue;
import value.IntValue;

public class ASTDif implements ASTNode<IValue>{
	
	private ASTNode<IValue> right;
	private ASTNode<IValue> left;
	
	public ASTDif (ASTNode<IValue> left, ASTNode<IValue> right) {
		this.right = right;
		this.left = left;
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> e, MemoryManagement mem) throws TypeErrorException, UnmatchedIdentifierException {
		IValue v1 = left.eval(e, mem);
		IValue v2 = right.eval(e, mem);
		return new BoolValue(!(v1.equals(v2)).getValue());
	}

	@Override
	public ASTType typecheck(IEnvironment<ASTType> e) throws TypeErrorException {
		ASTType t = ASTIntType.thisType;
		ASTType t2 = ASTBoolType.thisType;
		
		if((right.typecheck(e).equals(t) && left.typecheck(e).equals(t)) ||
				right.typecheck(e).equals(t2) && left.typecheck(e).equals(t2))
					return t2;
		
		throw new TypeErrorException();
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		String labelTrue = code.label();
		String labelFalse = code.label();
		
		left.compile(code, env);
		right.compile(code, env);
		code.emit("isub");
		code.emit("ifeq", labelFalse);
		code.emit_bool(true);
		code.emit("goto", labelTrue);
		code.emit_label(labelFalse); code.emit_bool(false);
		code.emit_label(labelTrue);
	}
}
