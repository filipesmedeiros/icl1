package memory;

import value.IValue;

public class MemoryManagement {
	
	public MemoryManagement() {
		
	}
	
	public MemoryCell create(IValue value) {
		return new MemoryCell(value);
	}

	public IValue get(MemoryCell ref) {
		return ((MemoryCell) ref).getValue();
	}
	public IValue set(MemoryCell ref, IValue value) {
		return ((MemoryCell) ref).setValue(value);
	}

	public void free(MemoryCell ref) {}
}
