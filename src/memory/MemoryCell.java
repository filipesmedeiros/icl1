package memory;

import utils.TypeErrorException;
import value.BoolValue;
import value.IValue;

public class MemoryCell implements IValue<IValue> {

	private IValue value;
	
	public MemoryCell(IValue value) {
		this.value = value;
	}
	
	@Override
	public BoolValue equals(IValue other) throws TypeErrorException {
		return value.equals(other);
	}

	@Override
	public IValue getValue() {
		return value;
	}
	
	public IValue setValue(IValue value) {
		this.value = value;
		return this.value;
	}

}
