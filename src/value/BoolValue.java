package value;

import utils.TypeErrorException;

public class BoolValue implements IValue<Boolean> {
	private boolean value;
	
	public BoolValue(boolean value) {
		this.value = value;
	}
	
	public Boolean getValue() {
		return value;
	}

	@Override
	public BoolValue equals(IValue<Boolean> other) throws TypeErrorException {
		if(other instanceof BoolValue)
			return new BoolValue(value == ((BoolValue) other).getValue());
		else
			throw new TypeErrorException();
	}
}
