package value;

import utils.TypeErrorException;

public interface IValue<T> {

	public BoolValue equals(IValue<T> other) throws TypeErrorException;
	
	public T getValue();
}
