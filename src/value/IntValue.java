package value;

import utils.TypeErrorException;

public class IntValue implements IValue<Integer> {
	private int value;
	
	public IntValue(int value) {
		this.value = value;
	}
	
	public Integer getValue() {
		return value;
	}

	@Override
	public BoolValue equals(IValue<Integer> other) throws TypeErrorException {
		if(other instanceof IntValue) {
			int dif = value - ((IntValue) other).getValue();
			
			if(dif != 0)
				return new BoolValue(false);
			else return new BoolValue(true);
		} else
			throw new TypeErrorException();
	}
}
