package value;

import java.util.List;

import ast.ASTNode;
import type.ASTType;
import utils.IEnvironment;
import utils.TypeErrorException;

public class Closure implements IValue {
	
	private List<Param> params;
	private ASTNode<IValue> body;
	private IEnvironment<IValue> e;
	
	public Closure(List<Param> params, ASTNode<IValue> body, IEnvironment<IValue> e) {
		this.params = params;
		this.body = body;
		this.e = e;
	}
	
	@Override
	public BoolValue equals(IValue other) throws TypeErrorException {
		return new BoolValue(false);
	}

	@Override
	public Object getValue() {
		return body;
	}
	
	public List<Param> getParams() {
		return params;
	}
	
	public IEnvironment<IValue> getE() {
		return e;
	}
	
	public ASTNode<IValue> getBody() {
		return body;
	}
	
	public static class Param {
		public String name;
		public ASTType type;
		
		public Param(String name, ASTType type) {
			this.name = name;
			this.type = type;
		}
	}
}
