package utils;

public class DuplicateIdentifierException extends Exception {

	private static final long serialVersionUID = 1L;

	public DuplicateIdentifierException() {
	}

	public DuplicateIdentifierException(String message) {
		super(message);
	}
	
}
