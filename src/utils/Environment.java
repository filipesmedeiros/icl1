package utils;

import java.util.ArrayList;

import ast.ASTNode;
import type.ASTType;
import utils.Assoc;


public class Environment<T> implements IEnvironment<T>{

	Environment<T> above;
	ArrayList<Assoc<T>> assocs;

	public Environment() {
		above = null;
		assocs = new ArrayList<Assoc<T>>();
	}

	private Environment(Environment<T> above) {
		assocs = new ArrayList<Assoc<T>>();
		this.above = above;
	}

	@Override
	public IEnvironment<T> beginScope() {
		return new Environment<T>(this);
	}

	@Override
	public IEnvironment<T> endScope() {
		return above;
	}

	@Override
	public void assoc(String id, T value) throws AlreadyExistsException {
		for(Assoc<T> assoc: assocs)
			if(assoc.id.equals(id))
				throw new AlreadyExistsException();

		assocs.add(new Assoc<T>(id, value));
	}

	@Override
	public T find(String id) throws UnmatchedIdentifierException {
		Environment <T> currentScope = this;
		while(currentScope != null) {
			for(Assoc<T> assoc : currentScope.assocs)
				if(assoc.id.equals(id))
					return assoc.value;
			currentScope = currentScope.above;
		}
		
		throw new UnmatchedIdentifierException("No variables with the given name found in any environment.");
	}

}
