package utils;

import ast.ASTNode;
import type.ASTType;

public class Assoc<T> {
	public String id;
	public T value;
	
	public Assoc(String id, T value){
		this.id = id;
		this.value = value;
	}
}
