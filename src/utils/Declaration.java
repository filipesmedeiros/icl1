package utils;

import ast.ASTNode;
import type.ASTType;

public class Declaration {
	
	public String id;
	public ASTNode value;
	public ASTType type;
	
	public Declaration(String id, ASTNode value, ASTType type) {
		this.id = id;
		this.value = value;
		this.type = type;
	}
}
