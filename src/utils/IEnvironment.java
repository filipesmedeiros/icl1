package utils;

import ast.ASTNode;
import type.ASTType;

public interface IEnvironment<T> {
	IEnvironment<T> beginScope();
	
	IEnvironment<T> endScope();
	
	void assoc(String id, T value) throws AlreadyExistsException;
	
	T find(String id) throws UnmatchedIdentifierException;
}
