package utils;

public class UnmatchedIdentifierException extends Exception {

	private static final long serialVersionUID = 1L;

	public UnmatchedIdentifierException() {
	}

	public UnmatchedIdentifierException(String message) {
		super(message);
	}
	
}
