package type;

public class ASTRefType implements ASTType {
	
	public final ASTType inner;
	
	public ASTRefType(ASTType inner) {
		this.inner = inner;
	}
	
	@Override
	public boolean equals(Object other) {
		if(inner instanceof ASTIntType) {
			if(other instanceof ASTIntType)
				return true;
		}
		
		if(inner instanceof ASTBoolType) {
			if(other instanceof ASTBoolType)
				return true;
		}
		
		if(inner instanceof ASTFunType) {
			if(other instanceof ASTFunType)
				return inner.equals((ASTFunType) other);
		}
		
		if(!(other instanceof ASTRefType))
			return false;
		
		ASTRefType ref = (ASTRefType) other;
		
		return ref.inner.equals(inner);
	}
}
