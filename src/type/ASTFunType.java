package type;

import java.util.LinkedList;
import java.util.List;

public class ASTFunType implements ASTType {
	
	public List<ASTType> params;
	public ASTType ret;
	
	public ASTFunType() {
		params = new LinkedList<>();
	}
	
	public ASTFunType(List<ASTType> params, ASTType ret) {
		this.params = params;
		this.ret = ret;
	}
	
	public void addParam(ASTType param) {
		params.add(param);
	}

	public void setRet(ASTType ret) {
		this.ret = ret;
	}
	
	@Override
	public boolean equals(Object other) {
		if(!(other instanceof ASTFunType))
			return false;
		
		ASTFunType fun = (ASTFunType) other;
		
		for(int i = 0; i < params.size(); i++)
			if(!params.get(i).equals(fun.params.get(i)))
				return false;
		
		return ret.equals(fun.ret);
	}
}
