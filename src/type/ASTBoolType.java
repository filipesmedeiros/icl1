package type;

public class ASTBoolType implements ASTType {

	public static final ASTBoolType thisType = new ASTBoolType();
	
	private ASTBoolType(){ }
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof ASTRefType)
			if(!((ASTRefType) other).inner.equals(thisType))
				return false;
			else
				return true;
		
		if(other instanceof ASTBoolType)
			return true;
		
		return false;
	}
}
