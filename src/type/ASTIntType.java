package type;

public class ASTIntType implements ASTType {

	public static final ASTIntType thisType = new ASTIntType();
	
	private ASTIntType(){ }
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof ASTRefType)
			if(!((ASTRefType) other).inner.equals(thisType))
				return false;
			else
				return true;
		
		if(other instanceof ASTIntType)
			return true;
		
		return false;
	}
}
