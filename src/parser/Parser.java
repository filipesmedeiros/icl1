/* Generated By:JavaCC: Do not edit this line. Parser.java */
package parser;
import utils.*;
import memory.*;
import value.*;
import value.Closure.Param;
import ast.*;
import type.*;
import java.util.LinkedList;
import compiler.*;

public class Parser implements ParserConstants {
  public static void main (String args[]) throws TypeErrorException , UnmatchedIdentifierException{
      Parser parser = new Parser(System.in);
      for (;;)    {
        System.out.print("> ");
        try      {
                ASTNode<IValue> n = parser.Start();
                        IEnvironment<IValue> vEnv = new Environment<IValue>();
                        IEnvironment<ASTType> tEnv = new Environment<ASTType>();
                        IEnvironment<Address> aEnv = new Environment<Address>();

                        n.typecheck(tEnv);

                        CodeBlock block = new CodeBlock();
                        n.compile(block, aEnv);
                        block.dump("output.txt");

                        System.out.println("OK! = "+ n.eval(vEnv, new MemoryManagement()).getValue());
        }  catch (Exception x) {
          System.err.println("Syntax Error -> " + x.getMessage());
          x.printStackTrace();
          parser.ReInit(System.in);
        }
   }
 }

  static final public ASTNode Start() throws ParseException {
        ASTNode t = null;
    t = Seq();
    jj_consume_token(QUIT);
                             {if (true) return t;}
    throw new Error("Missing return statement in function");
  }

  static final public ASTNode Seq() throws ParseException {
  Token sep;
  ASTNode comp, seq;
    comp = Comp();
    label_1:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case SEMICOLON:
        ;
        break;
      default:
        jj_la1[0] = jj_gen;
        break label_1;
      }
      sep = jj_consume_token(SEMICOLON);
      seq = Seq();
                                                          comp = new ASTSeq(comp, seq);
    }
          {if (true) return comp;}
    throw new Error("Missing return statement in function");
  }

  static final public ASTNode Comp() throws ParseException {
        ASTNode f1, f2;
        Token comp;
    f1 = Exp();
    label_2:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case EQUALS:
      case DIF:
      case GREATER:
      case LESS:
      case EQ_MORE:
      case EQ_LESS:
        ;
        break;
      default:
        jj_la1[1] = jj_gen;
        break label_2;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case EQUALS:
        comp = jj_consume_token(EQUALS);
        break;
      case DIF:
        comp = jj_consume_token(DIF);
        break;
      case GREATER:
        comp = jj_consume_token(GREATER);
        break;
      case LESS:
        comp = jj_consume_token(LESS);
        break;
      case EQ_MORE:
        comp = jj_consume_token(EQ_MORE);
        break;
      case EQ_LESS:
        comp = jj_consume_token(EQ_LESS);
        break;
      default:
        jj_la1[2] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      f2 = Exp();
                    if (comp.kind == EQUALS)
                                f1 = new ASTEqual(f1,f2);
                   else if ( comp.kind == DIF)
                                f1 = new ASTDif(f1,f2);
                   else if (comp.kind == GREATER)
                                f1 = new ASTMoreThan(f1,f2);
                   else if (comp.kind == LESS)
                                f1 = new ASTLessThan(f1,f2);
                   else if (comp.kind == EQ_MORE)
                                f1 = new ASTMoreEqThan(f1,f2);
                   else if (comp.kind == EQ_LESS)
                                f1 = new ASTLessEqThan(f1,f2);
    }
       {if (true) return f1;}
    throw new Error("Missing return statement in function");
  }

  static final public ASTNode Exp() throws ParseException {
        ASTNode f1, f2;
        Token comp;
    f1 = Term();
    label_3:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case PLUS:
      case MINUS:
      case OR:
        ;
        break;
      default:
        jj_la1[3] = jj_gen;
        break label_3;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case PLUS:
        comp = jj_consume_token(PLUS);
        break;
      case MINUS:
        comp = jj_consume_token(MINUS);
        break;
      case OR:
        comp = jj_consume_token(OR);
        break;
      default:
        jj_la1[4] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      f2 = Exp();
                    if (comp.kind == PLUS)
                                f1 = new ASTAdd(f1,f2);
                   else if ( comp.kind == MINUS)
                                f1 = new ASTSub(f1,f2);
                   else if (comp.kind == OR)
                                f1 = new ASTOr(f1,f2);
    }
       {if (true) return f1;}
    throw new Error("Missing return statement in function");
  }

  static final public ASTNode Term() throws ParseException {
        ASTNode f1, f2, exp;
        LinkedList<ASTNode> al;
        Token comp, id;
    f1 = Fact();
    label_4:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case TIMES:
      case DIV:
      case LPAR:
      case AND:
      case ATTRIB:
        ;
        break;
      default:
        jj_la1[5] = jj_gen;
        break label_4;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case TIMES:
        comp = jj_consume_token(TIMES);
        f2 = Term();
                                                 {if (true) return new ASTMult(f1,f2);}
        break;
      case DIV:
        comp = jj_consume_token(DIV);
        f2 = Term();
                                      {if (true) return new ASTDiv(f1,f2);}
        break;
      case AND:
        comp = jj_consume_token(AND);
        f2 = Term();
                                       {if (true) return new ASTAnd(f1,f2);}
        break;
      case ATTRIB:
        comp = jj_consume_token(ATTRIB);
        f2 = Exp();
                                          {if (true) return new ASTAttrib(f1, f2);}
        break;
      case LPAR:
        f2 = Apply(f1);
                            {if (true) return f2;}
        break;
      default:
        jj_la1[6] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
    }
           {if (true) return f1;}
    throw new Error("Missing return statement in function");
  }

  static final public ASTNode Fact() throws ParseException {
  Token t1, t3, t4, t5, t2, t6;
  ASTNode fact, exp1, exp2, exp3, exp4, exp5;
  ASTType type1;
  LinkedList<ASTNode> al;
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case Num:
      t1 = jj_consume_token(Num);
                fact = new ASTNum(Integer.parseInt(t1.image)); {if (true) return fact;}
      break;
    case LPAR:
      jj_consume_token(LPAR);
      fact = Comp();
      jj_consume_token(RPAR);
                                 {if (true) return fact;}
      break;
    case MINUS:
      t1 = jj_consume_token(MINUS);
      fact = Fact();
                                  fact = new ASTNegative(fact); {if (true) return fact;}
      break;
    case TRUE:
      t1 = jj_consume_token(TRUE);
                 fact = new ASTBool(Boolean.parseBoolean(t1.image)); {if (true) return fact;}
      break;
    case FALSE:
      t1 = jj_consume_token(FALSE);
                  fact = new ASTBool(Boolean.parseBoolean(t1.image)); {if (true) return fact;}
      break;
    case NOT:
      t1 = jj_consume_token(NOT);
      fact = Fact();
                              fact = new ASTNot(fact); {if (true) return fact;}
      break;
    case IF:
      t1 = jj_consume_token(IF);
      exp1 = Comp();
      t2 = jj_consume_token(THEN);
      exp2 = Seq();
      t3 = jj_consume_token(ELSE);
      exp3 = Seq();
      t4 = jj_consume_token(END);
                                                                                                  {if (true) return new ASTIf(exp1, exp2, exp3);}
      break;
    case WHILE:
      t1 = jj_consume_token(WHILE);
      exp1 = Comp();
      t2 = jj_consume_token(DO);
      exp2 = Seq();
      t4 = jj_consume_token(END);
                                                                        {if (true) return new ASTWhile(exp1, exp2);}
      break;
    case LET:
      t1 = jj_consume_token(LET);
                  ASTLet let = new ASTLet();
      label_5:
      while (true) {
        t2 = jj_consume_token(Id);
        t3 = jj_consume_token(TYPEDEF);
        type1 = Type();
        t4 = jj_consume_token(EQ);
        exp1 = Seq();
     let.newVariable(t2.image, exp1, type1);
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case Id:
          ;
          break;
        default:
          jj_la1[7] = jj_gen;
          break label_5;
        }
      }
      t5 = jj_consume_token(IN);
      exp3 = Seq();
                                                                            let.giveIn(exp3);
      t6 = jj_consume_token(END);
                                                                                                               {if (true) return let;}
      break;
    case NEW:
      t1 = jj_consume_token(NEW);
      fact = Fact();
                                {if (true) return new ASTInit(fact);}
      break;
    case REFER:
      t1 = jj_consume_token(REFER);
      fact = Fact();
                                  {if (true) return new ASTRefer(fact);}
      break;
    case FUN:
      t1 = jj_consume_token(FUN);
                  ASTFun fun = new ASTFun();
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case Id:
        t2 = jj_consume_token(Id);
        t5 = jj_consume_token(TYPEDEF);
        type1 = Type();
                                                                                              fun.giveParam(new Param(t2.image, type1));
        label_6:
        while (true) {
          switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
          case COMMA:
            ;
            break;
          default:
            jj_la1[8] = jj_gen;
            break label_6;
          }
          jj_consume_token(COMMA);
          t3 = jj_consume_token(Id);
          t5 = jj_consume_token(TYPEDEF);
          type1 = Type();
                                                            fun.giveParam(new Param(t3.image, type1));
        }
        break;
      default:
        jj_la1[9] = jj_gen;
        ;
      }
      t4 = jj_consume_token(DEF);
      exp1 = Seq();
                               fun.giveBody(exp1);
      jj_consume_token(END);
                                                               {if (true) return fun;}
      break;
    case Id:
      t1 = jj_consume_token(Id);
                 {if (true) return new ASTId(t1.image);}
      break;
    case PRINT:
      jj_consume_token(PRINT);
      exp1 = Exp();
                            {if (true) return new ASTPrint(exp1) ;}
      break;
    default:
      jj_la1[10] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    throw new Error("Missing return statement in function");
  }

  static final public ASTNode Apply(ASTNode f) throws ParseException {
  ASTNode exp5, exp4, exp2;
    ASTApply fun2 = new ASTApply(f);
    jj_consume_token(LPAR);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case Num:
    case MINUS:
    case LPAR:
    case TRUE:
    case FALSE:
    case NOT:
    case IF:
    case WHILE:
    case LET:
    case NEW:
    case FUN:
    case REFER:
    case PRINT:
    case Id:
      exp5 = Seq();
                                                                fun2.addArg(exp5);
      label_7:
      while (true) {
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case COMMA:
          ;
          break;
        default:
          jj_la1[11] = jj_gen;
          break label_7;
        }
        jj_consume_token(COMMA);
        exp4 = Seq();
                  fun2.addArg(exp4);
      }
      break;
    default:
      jj_la1[12] = jj_gen;
      ;
    }
    jj_consume_token(RPAR);
                                                      {if (true) return fun2;}
    throw new Error("Missing return statement in function");
  }

  static final public ASTType Type() throws ParseException {
  ASTType t1, type;
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case INT:
      jj_consume_token(INT);
            {if (true) return ASTIntType.thisType;}
      break;
    case BOOL:
      jj_consume_token(BOOL);
             {if (true) return ASTBoolType.thisType;}
      break;
    case LPAR:
      jj_consume_token(LPAR);
             ASTFunType fun = new ASTFunType();
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case LPAR:
      case REF:
      case INT:
      case BOOL:
        t1 = Type();
                                                                 fun.addParam(t1);
        label_8:
        while (true) {
          switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
          case COMMA:
            ;
            break;
          default:
            jj_la1[13] = jj_gen;
            break label_8;
          }
          jj_consume_token(COMMA);
          t1 = Type();
                                                                                                             fun.addParam(t1);
        }
        break;
      default:
        jj_la1[14] = jj_gen;
        ;
      }
      jj_consume_token(RPAR);
      t1 = Type();
                         fun.setRet(t1); {if (true) return fun;}
      break;
    case REF:
      jj_consume_token(REF);
      type = Type();
                          {if (true) return new ASTRefType(type);}
      break;
    default:
      jj_la1[15] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    throw new Error("Missing return statement in function");
  }

  static private boolean jj_initialized_once = false;
  /** Generated Token Manager. */
  static public ParserTokenManager token_source;
  static SimpleCharStream jj_input_stream;
  /** Current token. */
  static public Token token;
  /** Next token. */
  static public Token jj_nt;
  static private int jj_ntk;
  static private int jj_gen;
  static final private int[] jj_la1 = new int[16];
  static private int[] jj_la1_0;
  static private int[] jj_la1_1;
  static {
      jj_la1_init_0();
      jj_la1_init_1();
   }
   private static void jj_la1_init_0() {
      jj_la1_0 = new int[] {0x0,0x3e8000,0x3e8000,0x1000180,0x1000180,0x800e00,0x800e00,0x0,0x20,0x0,0x52406940,0x20,0x52406940,0x20,0x800,0x800,};
   }
   private static void jj_la1_init_1() {
      jj_la1_1 = new int[] {0x4,0x0,0x0,0x0,0x0,0x8,0x8,0x2000,0x0,0x2000,0x3070,0x0,0x3070,0x0,0xc80,0xc80,};
   }

  /** Constructor with InputStream. */
  public Parser(java.io.InputStream stream) {
     this(stream, null);
  }
  /** Constructor with InputStream and supplied encoding */
  public Parser(java.io.InputStream stream, String encoding) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser.  ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    try { jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source = new ParserTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 16; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  static public void ReInit(java.io.InputStream stream) {
     ReInit(stream, null);
  }
  /** Reinitialise. */
  static public void ReInit(java.io.InputStream stream, String encoding) {
    try { jj_input_stream.ReInit(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 16; i++) jj_la1[i] = -1;
  }

  /** Constructor. */
  public Parser(java.io.Reader stream) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser. ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    jj_input_stream = new SimpleCharStream(stream, 1, 1);
    token_source = new ParserTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 16; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  static public void ReInit(java.io.Reader stream) {
    jj_input_stream.ReInit(stream, 1, 1);
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 16; i++) jj_la1[i] = -1;
  }

  /** Constructor with generated Token Manager. */
  public Parser(ParserTokenManager tm) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser. ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 16; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(ParserTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 16; i++) jj_la1[i] = -1;
  }

  static private Token jj_consume_token(int kind) throws ParseException {
    Token oldToken;
    if ((oldToken = token).next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    if (token.kind == kind) {
      jj_gen++;
      return token;
    }
    token = oldToken;
    jj_kind = kind;
    throw generateParseException();
  }


/** Get the next Token. */
  static final public Token getNextToken() {
    if (token.next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    jj_gen++;
    return token;
  }

/** Get the specific Token. */
  static final public Token getToken(int index) {
    Token t = token;
    for (int i = 0; i < index; i++) {
      if (t.next != null) t = t.next;
      else t = t.next = token_source.getNextToken();
    }
    return t;
  }

  static private int jj_ntk() {
    if ((jj_nt=token.next) == null)
      return (jj_ntk = (token.next=token_source.getNextToken()).kind);
    else
      return (jj_ntk = jj_nt.kind);
  }

  static private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
  static private int[] jj_expentry;
  static private int jj_kind = -1;

  /** Generate ParseException. */
  static public ParseException generateParseException() {
    jj_expentries.clear();
    boolean[] la1tokens = new boolean[46];
    if (jj_kind >= 0) {
      la1tokens[jj_kind] = true;
      jj_kind = -1;
    }
    for (int i = 0; i < 16; i++) {
      if (jj_la1[i] == jj_gen) {
        for (int j = 0; j < 32; j++) {
          if ((jj_la1_0[i] & (1<<j)) != 0) {
            la1tokens[j] = true;
          }
          if ((jj_la1_1[i] & (1<<j)) != 0) {
            la1tokens[32+j] = true;
          }
        }
      }
    }
    for (int i = 0; i < 46; i++) {
      if (la1tokens[i]) {
        jj_expentry = new int[1];
        jj_expentry[0] = i;
        jj_expentries.add(jj_expentry);
      }
    }
    int[][] exptokseq = new int[jj_expentries.size()][];
    for (int i = 0; i < jj_expentries.size(); i++) {
      exptokseq[i] = jj_expentries.get(i);
    }
    return new ParseException(token, exptokseq, tokenImage);
  }

  /** Enable tracing. */
  static final public void enable_tracing() {
  }

  /** Disable tracing. */
  static final public void disable_tracing() {
  }

}
