package compiler;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import utils.Declaration;
import value.Closure.Param;
import type.ASTBoolType;
import type.ASTIntType;
import type.ASTType;

public class CodeBlock {

    ArrayList<String> code;
    
    private int labelCounter;
    private int objCounter;
    
    private StackFrame currFrame;

    public CodeBlock() {
        code = new ArrayList<String>(100);
        labelCounter = 0;
        objCounter = 0;
        currFrame = null;
    }
    
    public String label() {
    	return "LB" + labelCounter++;
    }
    
    public void emit_dup() {
    	code.add("dup");
    }
    
    public void emit_checkcast(String obj) {
    	code.add("checkcast " + obj);
    }
    
    public void emit_new(String name) {
    	code.add("new " + name + "_" + objCounter++);
    }
    
    public void emit_invokeSpecial(String name) {
    	code.add("invokespecial " + name + "/<init>()V");
    }
    
    public void emit_putField(String name, String varName, String type) {
    	code.add("putfield " + name + "/" + varName + " " + type);
    }
    
    public StackFrame currentFrame() {
    	return currFrame;
    }
    
    public void emit_aload() {
    	code.add("aload 0");
    }
    
    public void emit_astore() {
    	code.add("astore 0");
    }
    
    public void emit_bool(boolean b) {
    	code.add("iconst_" + (b ? "1" : "0"));
    }

    public void emit_push(int n) {
        code.add("sipush "+n);
    }

    public void emit(String comm) {
    	code.add(comm);
    }
    
    public void emit(String comm, String label) {
    	code.add(comm + " " + label);
    }
   
    public void emit_label(String label) {
    	code.add(label + ":");
    }

    void dumpHeader(PrintStream out) {
        out.println(".class public Demo");
        out.println(".super java/lang/Object");
        out.println("");
        out.println(";");
        out.println("; standard initializer");
        out.println(".method public <init>()V");
        out.println("   aload_0");
        out.println("   invokenonvirtual java/lang/Object/<init>()V");
        out.println("   return");
        out.println(".end method");
        out.println("");
        out.println(".method public static main([Ljava/lang/String;)V");
        out.println("       ; set limits used by this method");
        out.println("       .limit locals 10");
        out.println("       .limit stack 256");
        out.println("");
        out.println("       ; setup local variables:");
        out.println("");
        out.println("       ;    1 - the PrintStream object held in java.lang.out");
        out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
        out.println("");
        out.println("       ; place your bytecodes here");
        out.println("       ; START");
        out.println("");
    }

    void dumpFooter(PrintStream out) {
        out.println("       ; END");
        out.println("");
        out.println("");
        out.println("       ; convert to String;");
        out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
        out.println("       ; call println ");
        out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
        out.println("");
        out.println("       return");
        out.println("");
        out.println(".end method");
    }

    void dumpCode(PrintStream out) {
        for( String s : code )
            out.println("       "+s);
    }

    public void dump(String filename) throws FileNotFoundException {
        PrintStream out = new PrintStream(new FileOutputStream(filename));
        dumpHeader(out);
        dumpCode(out);
        dumpFooter(out);
    }
    
    public static class Loc {
    	String name;
    	String type;
    	Object value;
    	
    	public Loc(String name, String type, Object value) {
    		this.name = name;
    		this.type = type;
    		this.value = value;
    	}
    }
    
    public static class Ref {
    	String name;
    	Loc loc;
    	
    	public Ref(String name, String lName, String type, Object value) {
    		this.name = name;
    		loc = new Loc(lName, type, value);
    	}
    }
    
    public static class StackFrame {
		
		public int id;
		public StackFrame up;
		public List<Loc> locs;
		
		public int getID() {
			return id;
		}
		
		public StackFrame getUp() {
			return up;
		}
		
		public void putField(String name, String type, Object value) {
			locs.add(new Loc(name, type, value));
		}
		
		StackFrame(int id, StackFrame up){
			this.id = id;
			this.up = up;
			locs = new LinkedList<>();
		}
		
		void dump() throws FileNotFoundException {
			PrintStream out = new PrintStream(new FileOutputStream("frame_"+id+".j"));
			out.println(".class frame_"+id);
			out.println(".super java/lang/Object");
			out.println("");
			if (up != null) {
				out.println(".field public SL Lframe_"+up.getID()+";");
			}
			String num = "00";
			int i = 0;
			if(locs !=null) {
			for(Loc decl: locs) {
				if (i > 9) {
					num = ""+i;
				}else {
					num = "0"+i;
				}
				System.out.println("frame "+decl.type);
				out.println(".field public loc_"+num+" "+toJasmin(decl.type));
				i++;
			}
			}
			
			/* i = 0;
			if(params !=null) {
			for(Param p: params) {
				if (i > 9) {
					num = ""+i;
				}else {
					num = "0"+i;
				}
				out.println(".field public loc_"+num+" "+toJasmin(p.type));
				i++;
			} 
			} */
			out.println("");

			out.println(".method public <init>()V");
			out.println("aload_0");
			out.println("invokenonvirtual java/lang/Object/<init>()V");
			out.println("return");
			out.println(".end method");
			
			out.close();
		}
	}
    
    public static String toJasmin(String type) {
		if (type.equals("int") || type.equals("bool")) {
			return "I";
		}
			return "L"+type+";";
	}
}
